#!/usr/bin/env python3
import logging
import re
import string
from decimal import Decimal
from typing import Optional, cast

import typer
from lxml import etree

logger = logging.getLogger(__name__)
logger.addHandler(logging.StreamHandler())
# logger.setLevel(logging.DEBUG)

app = typer.Typer()

namespaces = {
    'svg': "http://www.w3.org/2000/svg",
    'inkscape': "http://www.inkscape.org/namespaces/inkscape"
}


class Point:

    def __init__(self, x, y, prec_point=None):
        self.x = x
        self.y = y

    def __str__(self):
        return f"({self.x}, {self.y})"

    __repr__ = __str__


class MovePoint(Point):

    def __str__(self):
        return f"m {self.x:.2f} {self.y:.2f}"

    __repr__ = __str__


class LinePoint(Point):

    def __str__(self):
        return f"l {self.x:.2f} {self.y:.2f}"

    __repr__ = __str__


class BezierPoint(Point):

    def __init__(self, bx, by, cx, cy, *pos, **kwargs):
        super().__init__(*pos, **kwargs)
        self.bx = bx
        self.by = by
        self.cx = cx
        self.cy = cy

    @classmethod
    def smooth(cls, cx, cy, x, y, prec_point):
        if isinstance(prec_point, BezierPoint):
            bx = prec_point.x - (prec_point.cx - prec_point.x)
            by = prec_point.y - (prec_point.cy - prec_point.y)
        else:
            bx = prec_point.x
            by = prec_point.y

        return cls(bx, by, cx, cy, x, y)

    def __str__(self):
        return (f"b {self.bx:.2f} {self.by:.2f} {self.cx:.2f} "
                f"{self.cy:.2f} {self.x:.2f} {self.y:.2f}")

    __repr__ = __str__


mode_class = {
    'm': MovePoint,
    'l': LinePoint,
    'h': LinePoint,
    'v': LinePoint,
    'c': BezierPoint,
    's': BezierPoint.smooth,
}

mode_class.update({k.upper(): v for k, v in mode_class.items()})


read_mode_prog = re.compile(r"\s*([smclzhv])", re.IGNORECASE)
read_float_prog = re.compile(r"\s*(-?\d+(?:\.\d+)?)(?:e([+-]\d+))?,?")
read_exp_prog = re.compile(r"\s*(-)?e([+-]?\d+)")


class PathReader:

    def __init__(self, path):
        self.path = path

    def read_mode(self):
        match = read_mode_prog.match(self.path)
        if not match:
            logger.debug(f"mode not found in: {self.path}")
            return None
        else:
            self.path = self.path[match.end():]
            return match.group(1)

    def read_float(self):
        match = read_float_prog.match(self.path)
        if not match:
            raise RuntimeError(f"could not parse float in:\n{self.path}")

        self.path = self.path[match.end():]
        d = Decimal(match.group(1))

        if match.group(2) is not None:
            d *= Decimal(10)**int(match.group(2))

        return d

    def parse_path(self):
        mode = None

        prec_point = None
        while self.path:
            logger.debug(self.path[:50])
            nmode = self.read_mode()
            if nmode is not None:
                mode = nmode

            mode = cast(str, mode)
            if mode.lower() == 'z':
                continue

            logger.debug(self.path[:50])
            logger.debug(f"mode: {nmode}, {mode}")

            coord = []
            if mode.lower() == 'c':
                for _ in range(6):
                    coord.append(self.read_float())

            elif mode.lower() == 's':
                for _ in range(4):
                    coord.append(self.read_float())

            elif mode.lower() == 'h':
                coord.append(self.read_float())
                if mode == 'H':
                    coord.append(Decimal(prec_point.y))
                else:
                    coord.append(Decimal(0))

            elif mode.lower() == 'v':
                if mode == 'V':
                    coord.append(Decimal(prec_point.x))
                else:
                    coord.append(Decimal(0))
                coord.append(self.read_float())

            else:
                for _ in range(2):
                    coord.append(self.read_float())

            if prec_point is not None and mode in string.ascii_lowercase:
                coord = [new+prec for new, prec in zip(coord, (prec_point.x, prec_point.y)*3)]

            prec_point = mode_class[mode](*coord, prec_point=prec_point)

            logger.debug(str(prec_point))

            yield prec_point


class Path():

    def __init__(self, path):
        self.path = list(PathReader(path).parse_path())

    def ass_path(self):
        return ' '.join(map(str, self.path))


@app.command()
def main(
    file: typer.FileText = typer.Argument(..., help="SVG file"),
    origin: Optional[bool] = typer.Option(
        False, "--origin", "-o",
        help="Move drawing to the origin of the canvas"
    )
):
    """ Convert SVG paths to ASS paths """
    parser = etree.XMLParser(ns_clean=True)
    tree = etree.parse(file, parser)

    paths = tree.xpath("//svg:path",
                       namespaces=namespaces)
    for path in paths:
        path = Path(path.attrib['d'])
        if origin:
            min_x = min([point.x for point in path.path])
            min_y = min([point.y for point in path.path])

            for point in path.path:
                point.x -= min_x
                point.y -= min_y

        print(path.ass_path())


if __name__ == "__main__":
    app()
